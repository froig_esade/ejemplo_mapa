//
//  ViewController.swift
//  Ejemplo Mapa
//
//  Created by Francesc Roig i Feliu on 30/10/17.
//  Copyright © 2017 Francesc Roig i Feliu. All rights reserved.
//

// Privacy - Location When In Use Usage Description

import UIKit
import MapKit // Necesario para el uso del componente MKMapView.

// IMPORTANTE !!
// Es necesario añadir la directiva: 'Privacy - Location When In Use Usage Description'
// en Info.plist. De lo contrario no se visualizarà la posición actual del dispositivo.
// Si no se precisa conocer la posición del dispositivo (acceso al GPS) no hace falta
// añadir esta directiva de seguridad.


class ViewController: UIViewController { // Inicio del bloque principal.

    // Conexión del componente gráfico MKMapVies con la variable 'mapa'.
    @IBOutlet weak var mapa: MKMapView!
    
    // Declaración e inicialización del controlador del GPS.
    // (esta linea solo es necesaria si se desea mostrar la posición del dispositivo)
    var localizador = CLLocationManager()
    
    
    /*
     * Esta fucnión de ejecuta al inicio de la aplicación,
     * justo antes de aparecer la pantalla vinculada con este
     * controlador (ViewController).
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // /////////// Configuracion del mapa ///////////////
        
        // Define el punto central del mapa.
        let centro_mapa = CLLocationCoordinate2DMake(41.405880, 2.105497)
        
        // Define el tamaño de la zona a mostrar del mapa.
        let alcance_mapa = MKCoordinateSpanMake(0.2, 0.2)
        
        // Define la región que 'mapa' debe mostrar.
        let region = MKCoordinateRegion(center: centro_mapa, span: alcance_mapa)
        
        // Carga la región en el componente 'mapa'.
        mapa.setRegion(region, animated: true)
        
        // //////////  Añade localizaciones /////////////////
        
        // Añade localización 1.
        let marcador_1 = MKPointAnnotation()
        marcador_1.coordinate = CLLocationCoordinate2DMake(41.466989, 2.092133)
        marcador_1.title = "ESADE"
        marcador_1.subtitle = "Sant Cugat"
        mapa.addAnnotation(marcador_1)
        
        // Añade localización 2.
        let marcador_2 = MKPointAnnotation()
        marcador_2.coordinate = CLLocationCoordinate2DMake(41.391731, 2.111591)
        marcador_2.title = "ESADE"
        marcador_2.subtitle = "Barcelona"
        mapa.addAnnotation(marcador_2)
        
        // /////////////// Muestra posición actual (GPS) ///////////////
        
        // Lanza el mesaje de solicitud de permiso para el GPS.
        localizador.requestWhenInUseAuthorization()
        
        // Muestra el punto azul en el mapa (también en propidades del mapa).
        mapa.showsUserLocation = true
        
    } // final de la función 'viewDidLoad'.

} // Final del bloque principal.

